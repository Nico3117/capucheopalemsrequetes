const { Model, DataTypes } = require("sequelize");
import databaseConnection from "../services/database-connection";

const Requete = databaseConnection.define("requete", {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER,
      },
      titre: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      recompense: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      recompense_exp: { type: DataTypes.INTEGER, allowNull: true },
      duree: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      date_limite: {
        type: DataTypes.DATE,
      },
      nbr_personne_minimum: {
        type: DataTypes.INTEGER,
      },
      status_actuel: {
        type: DataTypes.TEXT,
      }
    }, {
        tableName: 'requete',
        timestamps: false
    });



(async () => {
    try {
        await databaseConnection.sync({ force: false });
    } catch (err) {
        console.log(err)
    }
})();

export default Requete
