const { Model, DataTypes } = require("sequelize");
import databaseConnection from "../services/database-connection";

const MetierClasse = databaseConnection.define("metier_classe", {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER,
      },
      label: {
        type: DataTypes.TEXT,
      }
    }, {
        tableName: 'metier_classe',
        timestamps: false
    });



(async () => {
    try {
        await databaseConnection.sync({ force: false });
    } catch (err) {
        console.log(err)
    }
})();


export default MetierClasse
