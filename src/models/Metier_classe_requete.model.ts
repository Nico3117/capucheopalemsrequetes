const { Model, DataTypes } = require("sequelize");
import databaseConnection from "../services/database-connection";
import MetierClasse from "./Metier_classe.model";
import Requete from "./Requete.model";

const MetierClasseRequete = databaseConnection.define("metier_classe_requete", {
    id_metier_classe: {
        type: DataTypes.INTEGER,
      },
      id_requete: {
        type: DataTypes.INTEGER,
      }
    }, {
        tableName: 'metier_classe_requete',
        timestamps: false
    });



(async () => {
    try {
        await databaseConnection.sync({ force: false });
    } catch (err) {
        console.log(err)
    }
})();
MetierClasseRequete.hasMany(Requete);
MetierClasseRequete.hasMany(MetierClasse);

export default MetierClasseRequete
