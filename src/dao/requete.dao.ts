import Requete from "../models/Requete.model";

export class RequeteDao {
    constructor() {
    }

    public async list() {
        return await Requete.findAll();
    }

    public async create(requete) {
        return await Requete.create(requete);
    }

    public async delete(requeteID: string) {
        const requete = await this.getByID(requeteID);
        if (requete) {
            await requete.destroy()
            return requeteID;
        }
    }

    public async update(id, modifiedRequete) {
        const requete = await this.getByID(id);
        if (requete) {
            await requete.update(modifiedRequete)
            await requete.save()
            return requete
        }
    }

    public async getByLogin(login: string) {
        return await Requete.findOne({
            where: {
                login
            }
        });
    }

    public async getByID(requeteID: string) {
        return await Requete.findOne({
            where: {
                id: requeteID
            }
        });
    }
}
