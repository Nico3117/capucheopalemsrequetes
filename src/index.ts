import express from 'express';
const app = express();
const port = 3000; // default port to listen
const dotenv = require('dotenv')
dotenv.config()

const cors = require('cors')
app.use(cors())

// middleware used to parse incoming requests with JSON payloads
app.use(express.json())

import requeteController from "./controllers/requete.controller";
app.use('/api/requete', requeteController)

// swagger configuration
const swaggerJSDoc = require('swagger-jsdoc');

const swaggerDefinition: any = {
    openapi: '3.0.0',
    info: {
        title: 'Capucheopale authentication',
        version: '1.0.0',
        description: 'This is REST API for Capucheopale authentication'
    },
    components: {
        securitySchemes: {
            jwt: {
                type: 'http',
                name: 'Authorization',
                scheme: 'bearer',
                in: 'header',
                bearerFormat: 'JWT',
            }
        }
    },

    security: [{
        jwt: []
    }],

    servers: [
        {
            url: `http://localhost:${port}`,
            description: 'Local server'
        }
    ]
};

const options = {
    swaggerDefinition,
    // Paths to files containing OpenAPI definitions
    apis: ['**/controllers/*.js', './controllers/*.ts'],
};

const swaggerSpec = swaggerJSDoc(options);
const swaggerUi = require('swagger-ui-express');

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.get('/swagger.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    res.send(swaggerSpec)
})

// start the Express server
app.listen(process.env.PORT || port);
