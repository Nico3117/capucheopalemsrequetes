import { RequeteDao } from '../dao/requete.dao';
import {NotFoundError} from "../errors/not-found.error";
import {ConflictError} from "../errors/conflict.error";

const uuid = require('uuid')

export class RequetesService {
    private requeteDAO: RequeteDao = new RequeteDao()

    public async getAllRequetes() {
        return await this.requeteDAO.list()
    }

    public async getRequete(requeteId: string) {
        const requete = await this.requeteDAO.getByID(requeteId);
        if (!requete) {
            throw new NotFoundError('unknown requete')
        }
        return requete
    }

    public async createRequete(requete) {
        if (!RequetesService.checkRequeteToCreateIsValid(requete)) {
            throw new Error('invalid requete');
        }

        return await this.requeteDAO.create(requete);
    }

    public async deleteRequete(requeteID: string) {
        const requete = await this.requeteDAO.getByID(requeteID);
        if (!requete) {
            throw new NotFoundError('unknown requete')
        }
        return await this.requeteDAO.delete(requeteID);
    }

    public async updateRequete(id, requete) {
        const existingRequete = await this.requeteDAO.getByID(id);
        if (!existingRequete) {
            throw new NotFoundError('unknown requete')
        }

        return await this.requeteDAO.update(id, requete)
    }

    public static checkRequeteToCreateIsValid(requete) {
        return requete && requete.titre && requete.description && requete.recompense && requete.recompense_exp
    }

}
