import { Router } from 'express';
import { RequetesService } from '../services/requetes.service';
const requeteController = Router();
import auth from '../middlewares/auth.middleware';
import {NotFoundError} from "../errors/not-found.error";
import {ConflictError} from "../errors/conflict.error";



const requetesService = new RequetesService();
/**
 * @openapi
 * /api/requete/:
 *   get:
 *     summary: Get requete
 *     tags:
 *        - Requete
 */
requeteController.get('/', [auth.verifyToken, auth.isAdmin], async (req, res) => {

    const requetes = await requetesService.getAllRequetes();
    console.log(requetes);
    res.status(200).send(requetes);
})
/**
 * @openapi
 * /api/requete/{requeteId}:
 *   post:
 *     summary: create requete
 *     tags:
 *        - Requete
 *     parameters:
 *        - name: requeteId
 *          in: path
 *          required: true
 *          dataType: string
 */
requeteController.post('/',[auth.verifyToken, auth.isAdmin], async (req, res) => {
    try {
        const requete = await requetesService.createRequete(req.body)
        res.status(200).send(requete)
    } catch (error) {
        if(error instanceof ConflictError) {
            res.status(409).send('Error while creating the request')
        } else {
            res.status(400).send(error.message)
        }
    }
})

/**
 * @openapi
 * /api/requete/{requeteId}:
 *   get:
 *     summary: Get requete
 *     tags:
 *        - Requete
 *     parameters:
 *        - name: requeteId
 *          in: path
 *          required: true
 *          dataType: string
 */
requeteController.get('/:requeteID', [auth.verifyToken], async (req, res) => {
   try {
        console.log(req.params)
       const requete = await requetesService.getRequete(req.params.requeteID);
       res.status(200).send(requete);
   } catch (error) {
       if (error instanceof NotFoundError) {
           res.status(404).send('requete inconnu')
       } else {
           res.status(400).send(error.message)
       }
   }
})

/**
 * @openapi
 * /api/requete/{requeteId}:
 *   put:
 *     summary: Edit requete
 *     tags:
 *        - Requete
 *     parameters:
 *        - name: requeteId
 *          in: path
 *          required: true
 *          dataType: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *               description:
 *                 type: string
 *               recompense:
 *                 type: number
 *               recompense_exp:
 *                 type: number
 *               duree:
 *                 type: number
 *               date_limite:
 *                 type: datetime
 *               nbr_personne:
 *                 type number
 *               status_actuel:
 *                 type: string
 */
requeteController.put('/:requeteID', [auth.verifyToken], async (req, res) => {
    try {

        const requete = await requetesService.updateRequete(req.params.requeteID, req.body);
        res.status(200).send(requete);

    } catch (error) {
        if (error instanceof NotFoundError) {
            res.status(404).send('requete inconnu')
        } else {
            res.status(400).send(error.message)
        }
    }
})

/**
 * @openapi
 * /api/requete/{requeteId}:
 *   delete:
 *     summary: Get requete
 *     tags:
 *        - Requete
 *     parameters:
 *        - name: requeteId
 *          in: path
 *          required: true
 *          dataType: string
 */
requeteController.delete('/:requeteID', [auth.verifyToken, auth.isAdmin], async (req: any, res) => {
    try {
        await requetesService.deleteRequete(req.params.requeteID)
    } catch (error) {
        if (error instanceof NotFoundError) {
            res.status(404).send('requete inconnu')
        } else {
            res.status(400).send(error.message)
        }
    }
})

export default requeteController;
